<?php
namespace Magora\AppiumDriver;

use \Magora\AppiumDriver\AppiumDriverBy as By;

abstract class BasePage
{
    /** @var \MobileTester $tester */
    protected $actor;

    /** @var By[] */
    protected $androidElements;

    /** @var By[] */
    protected $iOsElements;

    public function __construct(\MobileTester $I)
    {
        $this->actor = $I;
        $this->initElements();
    }

    /**
     * @param $name
     * @return By
     * @throws \Exception
     */
    public function __get($name)
    {
        $isAndroid = true;
        $elements = $isAndroid ? $this->androidElements : $this->iOsElements;
        if (!array_key_exists($name, $elements)) {
            throw new \Exception("Element '$name' is not defined");
        }
        return $elements[$name];
    }

    protected function initElements()
    {
        $this->androidElements = [];
        $this->iOsElements = [];
    }

}