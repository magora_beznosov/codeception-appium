<?php
namespace Magora\AppiumDriver;

use Appium\TestCase\Element;
use Codeception\Util\Shared\Asserts;

class AppiumDriver extends \Appium\AppiumDriver
{
    use Asserts;

    /**
     * @param \Magora\AppiumDriver\AppiumDriverBy $by
     * @param $value
     */
    public function fillField(AppiumDriverBy $by, $value)
    {
        $element = $this->findElement($by);
        $this->sendKeys($element, $value);
    }

    /**
     * @param \Magora\AppiumDriver\AppiumDriverBy $by
     */
    public function click(AppiumDriverBy $by)
    {
        $this->findElement($by)->click();
    }

    /**
     * @inheritdoc
     */
    public function hideKeyboard($data = [])
    {
        return parent::hideKeyboard($data);
    }

    public function see($text)
    {
        $pageSource = $this->getPageSource();
        $this->assertContains($text, htmlspecialchars_decode($pageSource));
    }

    /**
     * @param \Magora\AppiumDriver\AppiumDriverBy $by
     * @return Element
     */
    public function findElement(AppiumDriverBy $by)
    {
        $element = new Element($this->getDriver(), $this->getSessionUrl());
        return $element->by($by->getBy(), $by->getValue());
    }

    /**
     * @param \Magora\AppiumDriver\AppiumDriverBy $by
     * @return Element[]
     */
    public function findElements(AppiumDriverBy $by)
    {
        $element = new Element($this->getDriver(), $this->getSessionUrl());

        $criteria = (new \PHPUnit_Extensions_Selenium2TestCase_ElementCriteria($by->getBy()))->value($by->getValue());
        $elements = $element->elements($criteria);

        return $elements;
    }

    public function wait($sleep = 3)
    {
        sleep($sleep);
    }

    /**
     * @param \Magora\AppiumDriver\AppiumDriverBy $by
     * @param int $timeout
     * @return bool
     * @throws \Exception
     */
    public function waitForElement(AppiumDriverBy $by, $timeout = 30)
    {
        $time = microtime(1);
        while (true) {
            if (count($this->findElements($by)) > 0) {
                return true;
            } elseif (microtime(1) - $time <= $timeout) {
                sleep(1);
            } else {
                throw new \Exception("Could not find element {$by} within timeout $timeout");
            }
        };
    }

}
