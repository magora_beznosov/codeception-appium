<?php
namespace Magora\AppiumDriver;

class AppiumDriverBy
{
    /**
     * @var string
     */
    private $by;
    /**
     * @var string
     */
    private $value;

    protected function __construct($by, $value)
    {
        $this->by = $by;
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getBy()
    {
        return $this->by;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    public function __toString()
    {
        return "[by => '" . $this->by . '\', value => \'' . $this->value . "']";
    }

    /**
     * Locates elements whose class name contains the search value; compound class
     * names are not permitted.
     *
     * @param string $class_name
     * @return \Magora\AppiumDriver\AppiumDriverBy
     */
    public static function className($class_name)
    {
        return new static('class name', $class_name);
    }

    /**
     * Locates elements matching a CSS selector.
     *
     * @param string $css_selector
     * @return \Magora\AppiumDriver\AppiumDriverBy
     */
    public static function cssSelector($css_selector)
    {
        return new static('css selector', $css_selector);
    }

    /**
     * Locates elements whose ID attribute matches the search value.
     *
     * @param string $id
     * @return \Magora\AppiumDriver\AppiumDriverBy
     */
    public static function id($id)
    {
        return new static('id', $id);
    }

    /**
     * Locates elements whose NAME attribute matches the search value.
     *
     * @param string $name
     * @return \Magora\AppiumDriver\AppiumDriverBy
     */
    public static function name($name)
    {
        return new static('name', $name);
    }

    /**
     * Locates anchor elements whose visible text matches the search value.
     *
     * @param string $link_text
     * @return \Magora\AppiumDriver\AppiumDriverBy
     */
    public static function linkText($link_text)
    {
        return new static('link text', $link_text);
    }

    /**
     * Locates anchor elements whose visible text partially matches the search
     * value.
     *
     * @param string $partial_link_text
     * @return \Magora\AppiumDriver\AppiumDriverBy
     */
    public static function partialLinkText($partial_link_text)
    {
        return new static('partial link text', $partial_link_text);
    }

    /**
     * Locates elements whose tag name matches the search value.
     *
     * @param string $tag_name
     * @return \Magora\AppiumDriver\AppiumDriverBy
     */
    public static function tagName($tag_name)
    {
        return new static('tag name', $tag_name);
    }

    /**
     * Locates elements matching an XPath expression.
     *
     * @param string $xpath
     * @return \Magora\AppiumDriver\AppiumDriverBy
     */
    public static function xpath($xpath)
    {
        return new static('xpath', $xpath);
    }


}
